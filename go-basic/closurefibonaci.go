package main

import "fmt"



func fibonacci() func() int {
	current := 1
	previous := 0
	return func() int {
	    sum := previous
		previous, current = current , previous + current
		return sum
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}