package main

import (
	"fmt"
)

func main() {
	fmt.Println("Print Fibonacci")
	mangFibonacci := []int{}
	sum := 0
	previous := 0
	current := 1
	for i := 0; i < 10 ; i++ {
		printFibonacci(i + 1, previous)
		mangFibonacci = append(mangFibonacci, previous)
		sum = previous + current
		previous = current
		current = sum
	}

	fmt.Println("Print Arrray Fibonacci", mangFibonacci)

	tongGiaTriMang := 0

	for _,v := range mangFibonacci {
		tongGiaTriMang += v
	}

	fmt.Println("Print Tong gia tri cua mang Fibonacci", tongGiaTriMang)


}

func printFibonacci( i , n int) {
	fmt.Printf("So fibonacci thu %d la : %d \n", i ,n)
}