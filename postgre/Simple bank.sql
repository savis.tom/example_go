CREATE TABLE "accounts" (
  "id" bigserial PRIMARY KEY,
  "owner" varchar NOT NULL,
  "balance" bigint NOT NULL,
  "currency" varchar NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE TABLE "entries" (
  "id" bigserial PRIMARY KEY,
  "accounts_id" bigint,
  "amout" bigint NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE TABLE "tranfers" (
  "id" bigserial PRIMARY KEY,
  "form_accounts_id" bigint,
  "to_accounts_id" bigint,
  "amout" bigint NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE INDEX ON "accounts" ("owner");

CREATE INDEX ON "entries" ("accounts_id");

CREATE INDEX ON "tranfers" ("form_accounts_id");

CREATE INDEX ON "tranfers" ("to_accounts_id");

CREATE INDEX ON "tranfers" ("form_accounts_id", "to_accounts_id");

COMMENT ON COLUMN "tranfers"."amout" IS 'must be positive';

ALTER TABLE "entries" ADD FOREIGN KEY ("accounts_id") REFERENCES "accounts" ("id");

ALTER TABLE "tranfers" ADD FOREIGN KEY ("form_accounts_id") REFERENCES "accounts" ("id");

ALTER TABLE "tranfers" ADD FOREIGN KEY ("to_accounts_id") REFERENCES "accounts" ("id");
