package main

import (
	"io"
	"log"
	"os"
)

func main() {

    src := "words.txt"
    dst := "words2.txt"

    fin, err := os.Open(src)
    if err != nil {
        log.Fatal(err)
    }
    defer fin.Close()

    fout, err := os.Create(dst)
    if err != nil {
        log.Fatal(err)
    }
    defer fout.Close()

    _, err = io.Copy(fout, fin)

    if err != nil {
        log.Fatal(err)
    }
}