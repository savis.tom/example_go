package repository

import (
	models "api-repository/model"
)

type StudentRepository interface {
	Select() ([]*models.Student , error) // tránh việc tao ra bản copy của struct Student
	Insert(s *models.Student) (error)
}