// implement
package repoimpl

import (
	models "api-repository/model"
	repo "api-repository/repository"
	"database/sql"
	"fmt"
)

type StudentRepoImpl struct {
	Db *sql.DB
}

func NewStudentRepo(db *sql.DB) repo.StudentRepository {
	return &StudentRepoImpl {
		Db : db,
	}
}

func (s *StudentRepoImpl) Select() ([]*models.Student , error){
	students := make([]*models.Student, 0)

	rows, err := s.Db.Query("SELECT * FROM student")

	if err != nil {
		return students, err
	}

	for rows.Next() {
		student := models.Student{}
		// function Scan mapping id , fullname , gender , email
		err := rows.Scan(&student.ID, &student.FullName, &student.Gender , &student.Email)
		if err != nil {
			break
		}

		students = append(students, &student)

	}

	err = rows.Err()
	if err != nil {
		return students, err
	}

	return students, nil
}

func (u *StudentRepoImpl) Insert(student *models.Student) (error) {
	insertStatement := `
	INSERT INTO student (id, full_name, gender, email)
	VALUES ($1, $2, $3, $4)`

	_, err := u.Db.Exec(insertStatement, student.ID, student.FullName, student.Gender, student.Email)	
	if err != nil {
		fmt.Println("Record added: ", err)
		return err
	}

	fmt.Println("Record added: ", student)

	return nil
}