package model

// In hoa để export struct
type Student struct {
	ID int
	FullName string
	Gender string
	Email string
}