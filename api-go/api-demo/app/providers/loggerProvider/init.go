package loggerProvider

import (
	"fmt"
	"os"
	"path/filepath"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var Logger *zap.Logger

func Init() {
	fmt.Println("---------------------------------")

	// var err error

	if dir, err := os.Getwd(); err != nil {
		panic(err)
	} else if fi, err := os.Stat(filepath.Join(dir, "logs")); os.IsNotExist(err) || !fi.IsDir() {
		_ = os.MkdirAll(filepath.Join(dir, "logs"), 0755)
	}
		
	w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   "logs/log.yaml",
			MaxSize:    100, // megabytes
			MaxBackups: 3,
			MaxAge:     30, // days
	})
		
	encoder := zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	core := zapcore.NewCore(zapcore.NewJSONEncoder(encoder), w, zap.InfoLevel)
	Logger = zap.New(core, zap.Development())

}