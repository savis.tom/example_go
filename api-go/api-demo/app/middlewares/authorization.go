package middlewares

import (
	"demo/app/controllers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func AuthorizationMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken, _ := c.Cookie("access_token")
		if accessToken == "" {
			if c.Request.Header.Get("Authorization") == "" {
				c.SetCookie("access_token", "", -1, "/", c.Request.Host, true, false)
				c.SetCookie("refresh_token", "", -1, "/", c.Request.Host, true, false)
				c.SetCookie("scope", "", -1, "/", c.Request.Host, true, false)
				c.SetCookie("id_token", "", -1, "/", c.Request.Host, true, false)
				controllers.ResponseError(c, http.StatusUnauthorized, "Chưa đăng nhập", nil)
			}
			accessToken = c.Request.Header.Get("Authorization")[6:]
		}

		c.Next()
	}
}

func VerifyUser(c *gin.Context) error {
	// @TODO Kiểm tra user có tồn tại thì cập nhật thông tin ra, nếu chưa tồn tại thì báo lỗi vào DB

	
	c.Set("user", "")
	return nil
}
