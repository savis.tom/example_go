package main

import (
	"fmt"

	"demo/app/providers/loggerProvider"
	"demo/app/providers/routerProvider"

	"github.com/gin-gonic/gin"
)

func main() {
	fmt.Println("Api - demo")
	gin.DisableConsoleColor()

	r := gin.New()

	loggerProvider.Init()
	routerProvider.Init(r)

	r.Run(":3000")
	

}