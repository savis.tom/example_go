# Bài hướng dẫn sử dụng framework gin để thực hiện

Lệnh khởi tạo

```
go mod init ....
```

Lệnh để gọi một thư viện về là

```
go get ...
```

Trong phần này yêu cầu gọi các thư viện sau:

Gin: Gin là một web framework được viết bằng Go (Golang). Nó có một API giống như martini với hiệu suất nhanh hơn tới 40 lần nhờ có httprouter

```
go get -u github.com/gin-gonic/gin
```

để sử dụng

```
import "github.com/gin-gonic/gin"
```

validate value using

```
 github.com/go-playground/validator/v10
```
