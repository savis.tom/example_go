package main

import (
	"fmt"
	"net/http"

	"errors"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type ApiError struct {
    Field       string   `json:"field"`
    Message     string   `json:"message"`
}
var validate *validator.Validate
type Students struct {
	ID          string    `json:"id" validate:"required,min=1,max=4"`
	FullName    string    `json:"full_name" validate:"required"`
	Age         int       `json:"age"`
	Gender      bool      `json:"gender" validate:"required"`
	IDSv		string 	  `json:"id_sv" validate:"required,eqfield=id" `		
}



var students = []Students{
	{ID: "1", FullName: "Nguyễn Văn A" , Age: 20 , Gender: true, IDSv: "1"},
	{ID: "2", FullName: "Nguyễn Văn B" , Age: 20 , Gender: true, IDSv: "2"},
	{ID: "3", FullName: "Nguyễn Văn C" , Age: 20 , Gender: true, IDSv: "3"},
	{ID: "4", FullName: "Nguyễn Văn D" , Age: 20 , Gender: true, IDSv: "4"},
}

func checkoutStudent(c *gin.Context) {
	id, ok := c.GetQuery("id")

	if !ok {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Missing id query parameter."})
		return
	}

	student, err := getStudentById(id)

	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Student not found."})
		return
	}

	c.IndentedJSON(http.StatusOK, student)
}

func returnStudent(c *gin.Context) {
	id, ok := c.GetQuery("id")

	if !ok {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Missing id query parameter."})
		return
	}

	student, err := getStudentById(id)

	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Student not found."})
		return
	}

	c.IndentedJSON(http.StatusOK, student)
}

func studentById(c *gin.Context){
	id := c.Param("id")
	student, err := getStudentById(id)

	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Student not found" })
		return
	}

	c.IndentedJSON(http.StatusOK,student)
}

func getStudentById(id string) (*Students, error){
	for i,s := range students {
		if s.ID == id {
			return &students[i], nil
		}
	}
	return nil, errors.New("student not found")
}

func getStudents(c *gin.Context){
	c.IndentedJSON(http.StatusOK, students)
}

func createStudent(c *gin.Context){
	var newStudent Students
    fmt.Println(c.BindJSON(&newStudent) != nil)
    validate = validator.New()

	err := validate.Struct(newStudent)
	if err != nil {

		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return
		}

		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return
		}

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println(err.Namespace())
			fmt.Println(err.Field())
			fmt.Println(err.StructNamespace())
			fmt.Println(err.StructField())
			fmt.Println(err.Tag())
			fmt.Println(err.ActualTag())
			fmt.Println(err.Kind())
			fmt.Println(err.Type())
			fmt.Println(err.Value())
			fmt.Println(err.Param())
			fmt.Println()
		}

		// from here you can create your own error messages in whatever language you wish
		return
	}
	
				
	students = append(students, newStudent)

	c.IndentedJSON(http.StatusCreated, gin.H{ 
		"status": http.StatusCreated , 
		"message": "Thêm học sinh thành công", 
		"data": newStudent,
	})
}

func msgForTag(tag string) string {
    switch tag {
	default:
		return ""
    case "required":
        return "This field is required"
    case "email":
        return "Invalid email"
    }
}

func main(){
	router := gin.Default()
	router.GET("/students", getStudents)
	router.GET("/students/:id", studentById)
	router.POST("/students", createStudent)
	router.PATCH("/checkout", checkoutStudent)
	router.PATCH("/return", returnStudent)
	router.Run("localhost:3000")
}
